﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TestMvcSolution.Startup))]
namespace TestMvcSolution
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
